#!/bin/bash

# This script repacks the php/doc-base tarball to provide the
# expected build directory structure for the documetation build.
# It was based on sergiodj's Ubuntu telegraf d/watch scripts.
#
# It works with uscan v4 and assumes that it will be called like:
#
#   SCRIPT --upstream-version VERSION
#
# Authors: Sergio Durigan Junior <sergio.durigan@canonical.com>
#          Athos Coimbra Ribeiro <athos.ribeiro@canonical.com>

set -e
set -x
set -o pipefail

upstream_version="$2"
orig_tar=$(realpath "../php-doc_${upstream_version}.orig.tar.xz")
orig_dir="$PWD"
work_dir=$(mktemp -d)

cleanup ()
{
    cd "$orig_dir"
    rm -rf "$work_dir"
}

trap cleanup INT QUIT 0

printf "Unpacking tarball '$orig_tar' to '$work_dir'"

tar xf "$orig_tar" -C "$work_dir"
source_dir_name=$(ls -1 "$work_dir")
cd "$work_dir/$source_dir_name"
contents=$(ls -A)
mkdir doc-base
mv $contents doc-base
cd ..
tar cJf "$orig_tar" "$source_dir_name"

cleanup

exit 0
